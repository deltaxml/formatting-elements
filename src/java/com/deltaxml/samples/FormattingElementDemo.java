// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;

import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.DocumentComparator.ExtensionPoint;
import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStepHelper;

public class FormattingElementDemo {

  public static void main(String[] args) throws Exception {

    File input1= new File("input1.xml");
    File input2= new File("input2.xml");

    DocumentComparator comparator= new DocumentComparator();

    comparator.compare(input1, input2, new File("no-format-result.xml"));

    FilterStepHelper fsh= comparator.newFilterStepHelper();
    FilterChain formatMarker= fsh.newSingleStepFilterChain(new File("mark-formatting.xsl"), "format-marker");

    comparator.setExtensionPoint(ExtensionPoint.PRE_FLATTENING, formatMarker);

    comparator.compare(input1, input2, new File("format-result.xml"));
  }

}
