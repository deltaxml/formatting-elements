# Formatting Element Changes
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

Why inline formatting elements need special processing in order to minimise the impact that changing them has on the result file.

This document describes how to run the sample. For concept details see: [Formatting Element Changes](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/formatting-element-changes)

## DCP Sample
If you have Apache Ant installed, use the build script provided to run the sample. Simply type the following command to run the pipeline and produce the output files *result/DCP/no-format-result.xml* and *result/DCP/format-result.xml*.

	ant run-dcp
	
This sample uses Ant to drive XML Compare's built-in command-line interface along with the Document Comparator Pipelines (DCP) configuration file: formatting-elements.dcp
## Java Sample
If you have Ant installed, use the build script provided to run the sample. Simply type the following command to run the pipeline and produce the output files *no-format-result.xml* and *format-result.xml*.

	ant run-dc
	
To clean up the sample directory, run the following command in Ant.

	ant clean
	